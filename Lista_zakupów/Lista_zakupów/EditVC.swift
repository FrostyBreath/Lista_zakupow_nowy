//
//  EditVC.swift
//  Lista_zakupów
//
//  Created by Bartosz Kucharski on 21.12.2016.
//  Copyright © 2016 Bartosz Kucharski. All rights reserved.
//

import UIKit

protocol editItemDelegate {
    func userDidOkEdit (text: String)
    func userDidCancelEdit ()
}

class EditVC: UIViewController {
    
    var editDelegate: editItemDelegate?
    
    @IBOutlet weak var editLabel: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func okPressed(_ sender: Any) {
        if editLabel.text != nil {
            let data = editLabel.text
            editDelegate?.userDidOkEdit(text: data!)
        }
        
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        editDelegate?.userDidCancelEdit()
    }
}
