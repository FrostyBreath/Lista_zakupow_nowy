//
//  ShoppingItem.swift
//  Lista_zakupów
//
//  Created by Bartosz Kucharski on 20.12.2016.
//  Copyright © 2016 Bartosz Kucharski. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class ShoppingItem: Object{
  dynamic var text: String?
  dynamic var date: Date?
  dynamic var imageBool = false
  
    convenience init(text: String, date: Date) {
    self.init()
    self.text = text
    self.date = date
  }
}
