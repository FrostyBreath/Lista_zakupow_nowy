//
//  ListViewController.swift
//  Lista_zakupów
//
//  Created by Bartosz Kucharski on 15.12.2016.
//  Copyright © 2016 Bartosz Kucharski. All rights reserved.
//

import UIKit
import RealmSwift

class ListViewController: UIViewController, DataSentDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var text: String?
    var date: Date?
    var stringDate: String?
    //var listOfItems: ListOfShoppingItems?
    var listOfItems: Results<ShoppingItem>?
    let realm = try! Realm()
    var selectedRow: ShoppingItem?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func reloadData() {
        listOfItems = realm.objects(ShoppingItem.self)
        tableView.reloadData()
    }
    
    
    
    @IBAction func addBtnWasPressed(_ sender: UIBarButtonItem) {
        if let nextVC = storyboard?.instantiateViewController(withIdentifier: "newItemVC") as? NewPositionViewController {
            self.navigationController?.pushViewController(nextVC, animated: true)
            nextVC.delegate = self
            
        }
    }
    
    func userDidEnterDataOk(data: String, date:Date) {
        let newItem : ShoppingItem = ShoppingItem(text: data, date: date)
        newItem.imageBool = false
        
        try! realm.write {
            realm.add(newItem)
        }
        reloadData()
        _ = navigationController?.popViewController(animated: true)
    }
    func userDidCancel() {
        _ = self.navigationController?.popViewController(animated: true)
    }
}

extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        listOfItems = realm.objects(ShoppingItem.self)
        return (self.listOfItems?.count) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "shoppingItemCell") as! ShoppingItemCell
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        dateFormatter.dateStyle = .short
        stringDate = dateFormatter.string(from: (listOfItems?[indexPath.row].date!)!)
        
        if listOfItems?[indexPath.row].imageBool == true {
            cell.itemImageView.image = #imageLiteral(resourceName: "checkImage")
        }
        else {
            cell.itemImageView.image = nil
        }
        
        cell.itemNameLabel.text = listOfItems?[indexPath.row].text
        cell.itemDateLabel.text = stringDate
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let ui = listOfItems?[indexPath.row]
        
        let editAction = UITableViewRowAction(style: .normal, title: NSLocalizedString("listOfItems.rowActionEdit", comment: "Comment") , handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            if let destVC = self.storyboard?.instantiateViewController(withIdentifier: "editVC") as? EditVC {
                self.navigationController?.pushViewController(destVC, animated: true)
                destVC.editDelegate = self
                self.selectedRow = self.listOfItems?[indexPath.row]
            }
            
        })
        editAction.backgroundColor = UIColor.blue
        
        let deleteAction = UITableViewRowAction(style: .default, title: NSLocalizedString("listOfItems.rowActionDelete", comment: "Comment for translator") , handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            try! self.realm.write {
                self.realm.delete(ui!)
                self.reloadData()
            }
        })
        
        return [editAction,deleteAction]
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let ui = listOfItems?[indexPath.row]
        if listOfItems?[indexPath.row].imageBool == false{
            try! realm.write {
                ui?.imageBool = true
            }
        }
        else {
            try! realm.write {
                ui?.imageBool = false
            }
            
        }
        
        tableView.reloadData()
    }
    
}

extension ListViewController: editItemDelegate {
    
    func userDidOkEdit(text: String) {
        if !text.isEmpty  {
            try! realm.write {
                self.selectedRow?.text = text
                self.realm.add(self.selectedRow!)
            }
        }
        reloadData()
        _ = self.navigationController?.popViewController(animated: true)
    }
    func userDidCancelEdit() {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
}

